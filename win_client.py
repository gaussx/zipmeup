import sys
import socket
import ssl
import os
import time
from math import ceil
from os.path import isfile
from PyQt5.QtWidgets import QApplication, QWidget, QInputDialog, QLineEdit, \
    QFileDialog, QListView, QTreeView, QAbstractItemView, QPushButton, QVBoxLayout, \
    QProgressBar, QTextEdit, QLabel
from PyQt5.QtCore import Qt, QTimer, QMutex
from PyQt5.QtGui import QTextCursor

HOST = "127.0.0.1"
PORT = 1234


class App(QWidget):
    def __init__(self, host=HOST, port=PORT, title="ZIP ME UP", left=256, top=256, width=1024, height=256):
        super().__init__()
        self.window = QWidget()
        self.title = title
        self.left = left
        self.top = top
        self.width = width
        self.height = height
        self.ret = None
        self.progressBar = None
        self.textEditor = None
        self.button1 = None
        self.button2 = None
        self.initUI()
        self.client = Client(host, port, self.progressBar, self.textEditor)
        self.loop()

    def initUI(self):
        self.window.setWindowTitle(self.title)
        self.window.setGeometry(self.left, self.top, self.width, self.height)
        layout = QVBoxLayout()

        self.button1 = QPushButton(self)
        self.button1.setText("Single / Multiple directories")
        self.button1.clicked.connect(self.openFoldersNameDialog)
        layout.addWidget(self.button1)

        self.button2 = QPushButton(self)
        self.button2.setText("Single / Multiple files")
        self.button2.clicked.connect(self.openFilesNamesDialog)
        layout.addWidget(self.button2)

        self.progressBar = QProgressBar()
        self.progressBar.setValue(0)
        self.progressBar.setAlignment(Qt.AlignCenter)
        layout.addWidget(self.progressBar)

        self.textEditor = QTextEdit()
        self.textEditor.setReadOnly(True)
        self.textEditor.setLineWrapMode(QTextEdit.NoWrap)
        self.textEditor.setAlignment(Qt.AlignCenter)
        self.textEditor.insertPlainText("Pick your option")
        layout.addWidget(self.textEditor)

        self.window.setLayout(layout)

    def loop(self):
        self.window.show()
        sys.exit(app.exec())

    def buttonsDisable(self):
        self.button1.setEnabled(False)
        self.button2.setEnabled(False)

    def app_program(self, id):
        # id
        # "files" - single or multiple file selection
        # "dirs" - single or multiple directory selection
        self.client.handshake()
        self.client.client_program(self.ret, id)
        self.client.send()

        self.client.receive()

        self.client.disconnect()

    def openFoldersNameDialog(self):
        file_dialog = QFileDialog()
        file_dialog.setFileMode(QFileDialog.DirectoryOnly)
        file_dialog.setOption(QFileDialog.DontUseNativeDialog, True)
        file_view = file_dialog.findChild(QListView, 'listView')

        # to make it possible to select multiple directories:
        if file_view:
            file_view.setSelectionMode(QAbstractItemView.MultiSelection)
        f_tree_view = file_dialog.findChild(QTreeView)
        if f_tree_view:
            f_tree_view.setSelectionMode(QAbstractItemView.MultiSelection)

        if file_dialog.exec():
            paths = file_dialog.selectedFiles()
        if not paths:
            return False
        self.ret = paths
        # remove possibility for user to interact with app for 30 sec
        self.buttonsDisable()
        self.app_program("dirs")
        return paths

    # return list of every selected file (in the same directory)
    def openFilesNamesDialog(self):
        options = QFileDialog.Options()
        options |= QFileDialog.DontUseNativeDialog
        files, _ = QFileDialog.getOpenFileNames(self, "QFileDialog.getOpenFilesNames()", "",
                                                "All Files (*)", options=options)
        if not files:
            return False
        self.ret = files
        # remove possibility for user to interact with app for 30 sec
        self.buttonsDisable()
        self.app_program("files")
        return files


class Client:
    def __init__(self, host, port, progressBar, textEditor):
        self.host = host
        self.port = port
        self.path = None
        self.mainDir = None
        self.dirs = None
        self.files = None
        self.client_socket = None
        self.progressBar = progressBar
        self.textEditor = textEditor

    def handshake(self):
        setTextEdit(self.textEditor, "Connecting to server.")
        self.client_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.client_socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)

        context = ssl.SSLContext(protocol = ssl.PROTOCOL_TLS_CLIENT)
        context.check_hostname = False
        context.verify_mode = ssl.CERT_NONE
        
        # context.options |= ssl.OP_NO_TLSv1 | ssl.OP_NO_TLSv1_1
        # context.options = ssl.PROTOCOL_TLS_CLIENT
        
        # conn = context.wrap_socket(self.client_socket, server_hostname=HOST)
        self.client_socket = context.wrap_socket(sock = self.client_socket, server_side = False)

        # self.client_socket.bind((self.host, self.port))
        self.client_socket.connect((self.host, self.port))

    def disconnect(self):
        self.client_socket.close()

    def client_program(self, ret, id):
        # when server is ready and client tell what it wants to compress client is able to send it to server
        if ret:
            setTextEdit(self.textEditor, "Preparing directories and files to send.")
            # parDir absolute path will be path where compress file send from server will be saved (stored in self.path)
            print("Given absolute path/-s:")
            if not isArray(ret):
                self.path = os.path.abspath(os.path.join(ret, os.pardir))
                print(ret)
            else:
                self.path = os.path.abspath(os.path.join(ret[0], os.pardir))
                for file in ret:
                    print(file)
                print("===========================================")
                dirs, files = getFiles(ret)
                # add temporary directory as holder - parDir folder name
                if len(files) == 0:
                    for file in dirs:
                        files.append([
                            os.path.abspath(os.path.join(self.path, file)),
                            os.path.relpath(os.path.abspath(os.path.join(self.path, file)), os.path.join(self.path, os.pardir))
                        ])
                    # test for only files example
                    if id == "files":
                        dirs.clear()
                        dirs.append(os.path.relpath(os.path.basename(os.path.normpath(self.path))))
                    # if there was operation on directories it is possible for empty directory - which makes problems in files
                    # hold all directories, but remove its path from files list
                    elif id == "dirs":
                        tmpFiles = []
                        for fileAbs, fileRtv in files:
                            if os.path.basename(os.path.normpath(fileRtv)) not in dirs:
                                tmpFiles.append([fileAbs, fileRtv])
                        files = tmpFiles
                self.mainDir = os.path.basename(os.path.normpath(self.path))
                # dirs - holding all directories, first value is root, rest is relative path to root
                # files - holding every file [absolutePath, relativePath]
                # absolute path needed for client (send)
                # relative path needed for server (in which directory to send)
                print(dirs, files)
            print("ParDir absolute path => ", self.path)
            print("mainDir - name of compress file and main directory for everything => ", self.mainDir)
            print("===========================================")
            # if everything went alright client is ready to send everything to server
            self.files = files
            self.dirs = dirs
        else:
            print("Error - return value not exists!")
        return

    def send(self):
        # TODO send to server
        print("Number of directories: ", len(self.dirs))
        print("Name of compress file (parent for main directory): ", self.mainDir)
        print("Number of files: ", len(self.files))
        # send mainDir name - all directories and files will be stored in this directory\
        # print("Trying to send anything")
        # self.client_socket.send("1\\".encode("utf-8"))
        # print("Successfully sent")

        # send number of directories
        setTextEdit(self.textEditor, "Sending directories.")
        dir_count = str(len(self.dirs)) + "\\"
        print("Directory count:", dir_count)
        self.client_socket.send(dir_count.encode("utf-8"))
        # self.client_socket.send(str(len(self.dirs)).encode("utf-8"))

        # send tree of directories with relative paths
        for dir in self.dirs:
            dir_name = str(dir) + "\\"
            self.client_socket.send(dir_name.encode("utf-8"))

        # send name of main archive
        # main_archive = self.mainDir + "\\"
        # self.client_socket.send(main_archive.encode("utf-8"))

        # send number of files
        setTextEdit(self.textEditor, "Sending files.")
        file_number = str(len(self.files)) + "\\"
        self.client_socket.send(file_number.encode("utf-8"))

        # send file by file => size of file + file itself
        for fileAbs, fileRtv in self.files:
            fileRead(fileAbs, fileRtv, self.client_socket, self.progressBar, self.textEditor)

    def receive(self):
        # TODO receive from server
        # after send client is waiting for respond of server and get compress file
        message = ""
        request = self.client_socket.recv(256)
        request = request.decode("utf-8")
        message += request
        while request[-1] != '\\':
            request = self.client_socket.recv(256)
            request = request.decode("utf-8")
            message += request
        print(message)
        zip_size = int(message[:-1])
        remaining_bytes = zip_size
        with open("result.zip", "wb") as result:
            while remaining_bytes > 0:
                request = self.client_socket.recv(256)
                read_bytes = len(request)
                result.write(request)
                remaining_bytes -= read_bytes
        setTextEdit(self.textEditor, "File received! Name: result.zip. Check the folder containing this app.")



def setTextEdit(textEdit, text):
    textEdit.clear()
    textEdit.moveCursor(QTextCursor.End, QTextCursor.MoveAnchor)
    textEdit.insertPlainText(text)
    textEdit.setAlignment(Qt.AlignCenter)

def fileRead(fileAbs, fileRtv, socket, progressBar, textEditor):
    setTextEdit(textEditor, "Sending " + fileAbs + ".")
    # send relative path
    rel_name = str(fileRtv) + "\\"
    socket.send(rel_name.encode("utf-8"))
    # send size of file
    file_size = str(os.stat(fileAbs).st_size) + "\\"
    size = socket.send(file_size.encode("utf-8"))
    setTextEdit(textEditor, "Sending " + fileAbs + ".")
    # send file itself
    try:
        file = open(fileAbs, "rb")
    except OSError:
        print("Could not open / read file: ", fileAbs)
        sys.exit(-1)
    with file:
        suma = 0
        while True:
            bytes_read = file.read(256)
            # for el in bytes_read:
                # print("Character -> ", el, " <- value:", chr(el))
            suma += len(bytes_read)
            # progressBar.setValue(ceil(suma / size) * 100)
            progressBar.setValue(1)
            if not bytes_read:
                progressBar.setValue(0)
                setTextEdit(textEditor, "End of file " + fileAbs)
                break
            socket.send(bytes_read)


def isArray(absPaths):
    if type(absPaths) == list:
        return True
    return False


def getFiles(absPaths):
    dd = []
    ff = []
    for absPath in absPaths:
        dd.append(os.path.relpath(absPath, os.path.join(absPath, os.pardir)))
        for root, dirs, files in os.walk(absPath):
            for d in dirs:
                absolutePath = os.path.abspath(os.path.join(root, d))
                relativePath = os.path.relpath(absolutePath, os.path.join(absPath, os.pardir))
                print(relativePath)
                if isfile(absolutePath):
                    ff.append([absolutePath, relativePath])
                else:
                    dd.append(relativePath)
            for f in files:
                absolutePath = os.path.abspath(os.path.join(root, f))
                relativePath = os.path.relpath(absolutePath, os.path.join(absPath, os.pardir))
                print(relativePath)
                ff.append([absolutePath, relativePath])
    return dd, ff


if __name__ == '__main__':
    app = QApplication(sys.argv)
    myApp = App()
    try:
        sys.exit(app.exec())
    except SystemExit:
        print('closing Window...')
    exit(0)
