# ZIPMEUP

Zipmeup to prosty system oferujący zdalną kompresję plików. Podzielony jest na programy serwera i klienta. Klient wybiera na swoim komputerze pliki, które przesyła do serwera, a następnie odbiera archiwum w formacie .zip.

Projekt ten wykonany został z [Oskarem Kiliańczykiem](https://www.gitlab.com/oskkil), który stworzył klienta aplikacji.

Link do ikony projektu: [tutaj]("https://www.flaticon.com/free-icons/file")

# Programy zależne

Do poprawnego działania serwera, w systemie operacyjnym musi znajdować się program `zip`. Jeżeli nie jest zainstalowany, można go łatwo doinstalować w większości dystrybucji systemu GNU/Linux.
Przykładowe rozwiązania dla dystrybucji:
1. openSUSE
```
sudo zypper install zip
```
2. Debian
```
sudo apt install zip
```
3. Arch Linux
```
sudo pacman -S zip
```
W przypadku chęci odczytania pliku w systemie GNU/Linux należy zainstalować program `unzip`. Instaluje się go tak samo, jak `zip`.

Z kolei do poprawnego działania programu klienta należy mieć zainstalowany moduł PyQt5. Aby go zainstalować, należy wprowadzić poniższą linijkę do terminala (w systemie operacyjnym, w którym uprzednio zainstalowany Pythona):
```
python -m pip install pyqt5
```
# Protokół komunikacyjny
Protokół komunikacji pomiędzy klientem i serwerem jest oparty na protokole TLS/SSL. Sama wymiana danych odbywa się w następujący sposób:
1. Nawiązywane jest połączenie między klientem i serwerem (TLS/SSL).
2. Klient przesyła serwerowi informacje o liczbie folderów oraz ich nazwach.
3. Klient przesyła serwerowi informacje o liczbie plików, a następnie szereg informacji złożony z wielkości pliku, jego nazwy oraz jego zawartości.
4. Serwer przesyła klientowi informację o wielkości archiwum wynikowego oraz jego zawartość.
5. Po odebraniu archiwum klient zamyka połączenie.

# Opis implementacji

## Klient

Program klienta jest napisany w Pythonie, a GUI wykonane przy pomocy biblioteki Qt5. Plik zawiera dwie klasy:
1. App - utworzenie oraz obsługa interfejsu użytkownika,
2. Client - zarządzanie połączeniem z serwerem, wysyłanie/odbieranie danych.

Proste menu zbudowane przez klasę App daje nam wybór odnośnie rodzaju przesyłanych danych - można zdecydować się na pliki lub katalogi. 
Po wybraniu interesujących użytkownika plików klasa Client przygotowuje dane, które serwer wykorzystuje do odtworzenia struktury danych przesyłanej przez użytkownika (np. katalogu wraz z jego podfolderami i plikami).
Po wysłaniu danych oczekuje na odesłanie skompresowanego archiwum przez serwer, a po odebraniu go zamyka połączenie.


## Serwer

Serwer jest napisany w C. Zawiera implementacje funkcji służących do odczytu i zapisu z socketa do czasu otrzymania całego pliku lub znaku końca danych, w zależności od potrzeby.
Stosowane są dwa rodzaje odczytu - 
1. `inner_read()` - służący do odczytywania takich danych jak nazwa pliku, liczba przesyłanych bajtów itd. Odczyt kończy się na znaku końca danych - w naszym przypadku '\'.
2. `file_read()` - służący do odczytywania danych binarnych. Odczyt kończy się po odebraniu wcześniej określonej liczby bajtów.

A także dwa rodzaje zapisu:
1. `inner_write()` - na socket opisany w zmiennej `ssl` wysyła dane z bufora `buffer` tak długo, aż nie wyśle ich łącznie `SIZE`.
2. `file_write()` - na socket opisany w zmiennej `ssl` przepisuje bajty z bufora `file_contents` na socket przy użyciu bufora pomocniczego `buffer` tak długo, aż nie przepisze ich łącznie `size`.

Obsługa klientów odbywa się współbieżnie: wykorzystano w tym celu procesy potomne. Przy połączeniu z klientem serwer używa funkcji `fork()` w celu utworzenia procesu potomnego, który zajmuje się dalszą obsługą klienta.

# Sposób kompilacji i uruchomienia programów

## Klient
Wystarczy w środowisku z zainstalowanym Pythonem 3 przenieść się do folderu z kodem źródłowym i wydać polecenie `python win_client.py`.

## Serwer
Najprostszym sposobem jest zainstalowanie programu GNU Make (wiele dystrybucji GNU/Linux ma go odgórnie zainstalowanego), przeniesienie się do folderu z kodem źródłowym i wydanie polecenia `make`.
Polecenie to skompiluje i uruchomi plik wykonywalny serwera. Jeśli jednak nie ma możliwości skorzystania z GNU Make, można skompilować i uruchomić serwer poniższym poleceniem:
```
gcc -lssl -lcrypto -Wall server.c -o server && ./server
```