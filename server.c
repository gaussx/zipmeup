#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <fcntl.h>
#include <sys/sendfile.h>

#include <sys/types.h>
#include <sys/wait.h>
#include <sys/stat.h>
#include <signal.h>

#include <arpa/inet.h>
#include <netinet/in.h>
#include <netdb.h>

#include <openssl/ssl.h>
#include <openssl/rsa.h>

#define BUFSIZE 256

void execute(char* command, char* arguments){
    char* tmp_command = (char*)malloc(sizeof(char) * 1024);
    strcpy(tmp_command, command);
    strcat(tmp_command, " ");
    strcat(tmp_command, arguments);
    system(tmp_command);
    free(tmp_command);
}
char* zip(char* src, char* dest){
    char* tmp_command = (char*)malloc(sizeof(char) * 1024);
    strcpy(tmp_command, "zip -r ");
    strcat(tmp_command, dest);
    strcat(tmp_command, " ");
    strcat(tmp_command, src);
    strcat(tmp_command, " > /dev/null");
    system(tmp_command);
    free(tmp_command);
    return dest;
}

void flush_buffer(char* buffer, int size, char character){
    for(int i = 0; i < size; ++i){
        buffer[i] = character;
    }
}

int SSL_inner_read(SSL* ssl, char* buffer, int size){
    char temp_buffer[BUFSIZE];
    flush_buffer(temp_buffer, size, '\0');
    flush_buffer(buffer, size, '\0');
    int current_position = 0, read_bytes = 0;
    do{
        read_bytes = SSL_read(ssl, buffer, sizeof(buffer));
        strncat(temp_buffer, buffer, read_bytes);
        current_position += read_bytes;
        if(current_position >= BUFSIZE){
            break;
        }
    } while(buffer[read_bytes - 1] != '\\');
    strncpy(buffer, temp_buffer, current_position - 1);
    return current_position;
}

int SSL_inner_write(SSL* ssl, char* buffer, int size){
    int written_bytes = 0, current_position = 0;
    char temp_buffer[BUFSIZE];
    memset(temp_buffer, 0, sizeof(temp_buffer));
    while(current_position < size){
        written_bytes = SSL_write(ssl, buffer, size);
        strncat(temp_buffer, buffer, written_bytes);
        current_position += written_bytes;
    }
    return current_position;
}

int SSL_file_write(SSL* ssl, unsigned char* file_content, unsigned char* buffer, unsigned long long size){
    unsigned long long counter = 0, change = 0;
    unsigned long long remaining_bytes = size;
    while(remaining_bytes > 0){
        change = 0;
        for(unsigned long long i = counter; i < counter + sizeof(buffer); ++i){
            if(i >= size){
                break;
            }
            buffer[i - counter] = file_content[i];
            ++change;
        }
        remaining_bytes -= change;
        counter += change;
        SSL_write(ssl, buffer, change);
    }
    return counter;
}





int main(){
    //load ssl related content, certificates etc.
    SSL_library_init();
    SSL_load_error_strings();

    SSL_CTX* ctx = SSL_CTX_new(TLS_server_method());
    SSL* ssl;
    SSL_CTX_use_certificate_file(ctx, "server.crt", SSL_FILETYPE_PEM);
    SSL_CTX_use_PrivateKey_file(ctx, "server.key", SSL_FILETYPE_PEM);
    //clean up possible earlier server runs
    system("rm -rf users");
    system("mkdir users");
    
    int sfd, cfd, on = 1, bind_chk, rc;
    unsigned char file_buf[256];
    char buf[256];

    //set server-related constants, bind address, open socket
    socklen_t sl;
    struct sockaddr_in saddr, caddr;
    memset(&saddr, 0, sizeof(saddr));
    saddr.sin_family = AF_INET;
    saddr.sin_addr.s_addr = INADDR_ANY;
    saddr.sin_port = htons(1234);
    sfd = socket(PF_INET, SOCK_STREAM, IPPROTO_TCP);
    setsockopt(sfd, SOL_SOCKET, SO_REUSEADDR, (char*)&on, sizeof(on));
    bind_chk = bind(sfd, (struct sockaddr*) &saddr, sizeof(saddr));
    if(bind_chk != 0){
        printf("Failed to bind server address to socket.\n");
        exit(-1);
    }
    listen(sfd, 10);
    unsigned long long licznik = 0;
    while(1){
        //wait for a client to connect
        sl = sizeof(caddr);
        cfd = accept(sfd, (struct sockaddr*) &caddr, &sl);
        ++licznik;
        if(fork() == 0){
            //create ssl context and accept connection
            ssl = SSL_new(ctx);
            SSL_set_fd(ssl, cfd);
            SSL_accept(ssl);
            printf("new connection from %s:%d\n",
                    inet_ntoa((struct in_addr)caddr.sin_addr),
                    ntohs(caddr.sin_port));

            //create user directory based on client file descriptor
            char user_path[1024];
            char cfd_format[256];
            int cfd_length = sprintf(cfd_format, "%llu", licznik);
            strcpy(user_path, "users/");
            strncat(user_path, cfd_format, cfd_length);
            printf("%s\n", user_path);
            execute("mkdir", user_path);
            char user_file[1024];
            strcpy(user_file, user_path);

            //receive the directory count
            rc = SSL_inner_read(ssl, buf, sizeof(buf));
            int dir_number = atoi(buf);

            //receive the names and create the directories
            char dir_name[1024];
            for(int i = 0; i < dir_number; ++i){
                SSL_inner_read(ssl, buf, sizeof(buf));
                flush_buffer(dir_name, 1024, '\0');
                strcpy(dir_name, user_path);
                strcat(dir_name, "/");
                strcat(dir_name, buf);
                if(dir_name[strlen(dir_name) - 1] == '\\'){
                    dir_name[strlen(dir_name) - 1] = '\0';
                }
                execute("mkdir", dir_name);
            }

            //receive the file number
            rc = SSL_inner_read(ssl, buf, sizeof(buf));
            int file_number = atoi(buf);

            //for each file
            for(int i = 0; i < file_number; ++i){
                //receive the file name
                flush_buffer(buf, sizeof(buf), '\0');
                SSL_inner_read(ssl, buf, sizeof(buf));
                flush_buffer(user_file, sizeof(user_file), '\0');
                strcat(user_file, user_path);
                strcat(user_file, "/");
                strcat(user_file, buf);

                //receive the file size
                execute("touch", user_file);
                FILE* original_file = fopen(user_file, "wb");
                rc = SSL_inner_read(ssl, buf, sizeof(buf));
                unsigned long long file_size = atoll(buf);
                //receive the file
                int intercept_remaining = (unsigned long long)file_size;
                while(intercept_remaining > 0){
                    rc = SSL_read(ssl, file_buf, sizeof(buf));
                    intercept_remaining -= rc;
                    fwrite(file_buf, 1, rc, original_file);
                }
                fclose(original_file);
                printf("Successfully intercepted file! Name: %s\n", user_file);
            }

            //prepare the .zip archive
            char main_zip_dir[1024];
            strcpy(main_zip_dir, user_path);
            strcat(main_zip_dir, "/");
            strcat(user_path, "/zipped.zip");//src   dest
            char* returned_filename = zip(main_zip_dir, user_path);
            //read .zip archive's content into memory
            FILE* file_to_send = fopen(user_path, "rb");
            unsigned char *file_content;

            fseek(file_to_send, 0L, SEEK_END);
            long test_file_size = ftell(file_to_send);
            fseek(file_to_send, 0L, SEEK_SET);
            long size = (size_t)test_file_size;
            if ((long)size != test_file_size) {
                printf("file size too large for a single block: %ld\n", test_file_size);
                // fclose(file_to_test);
                fclose(file_to_send);
                return -1;
            }
            file_content = (unsigned char*)malloc(sizeof(unsigned char) * size);
            fread(file_content, 1, size, file_to_send);

            //send the file size to the client
            struct stat file_info;
            stat(returned_filename, &file_info);
            flush_buffer(buf, sizeof(buf), '\0');
            int length = sprintf(buf, "%ld", file_info.st_size);
            strcat(buf, "\\");
            SSL_inner_write(ssl, buf, ++length);
            memset(buf, -1, sizeof(buf));
            memset(file_buf, 255, sizeof(buf));

            //send over the archive
            SSL_file_write(ssl, file_content, file_buf, (long)size);
            printf("File successfully delivered to client!\n");

            //clean up the user directory
            execute("rm -rf", main_zip_dir);

            //free pointers
            if(returned_filename != NULL){
                free(returned_filename);
            }
            if(file_content != NULL)
                free(file_content);
            if(ssl != NULL)
                SSL_free(ssl);
            if(file_to_send != NULL)
                fclose(file_to_send);
            if(fcntl(cfd, F_GETFD) != -1)
                close(cfd);
            printf("User ended communication\n");
            exit(0);
        }
        else{
            if(fcntl(cfd, F_GETFD) != -1)
                close(cfd);
        }
    }
}